# Introduction

Reads the hunting_log from the game Hunter: Call of the Wild which is produced by Avalanche Studios. The hunting log contains basic information relating to the latest kills harvested, and approximately the top 40 harvests across all districts and regions. 


## Usage

```
node index <path to hunting_log>
```

The hunting_log by default is located at `~Documents/Avalanche Studios/theHunter Call of the Wild/Saves/<steam ID>/hunting_log`, where `<steam ID>` is you steam ID.