/**
Hunter: Call of the Wild Hunter Log reader

*/

const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const stat = util.promisify(fs.stat);
const chalk = require('chalk');
const log = require('loglevel');

const path = process.argv[2];
if (!path) process.exit(1);

const log1 = log.getLogger('l1');
log1.setLevel('DEBUG');


const log2 = log.getLogger('l2');
log2.setLevel('DEBUG');


class HCoW {
	constructor(path) {
		this.pos = 0;
		this.section = '';

		this.stats = {};
		this.stats.species = {};
		this.stats.furs = {};

		this.speciesSeen = [];
		this.fursSeen = [];

		this.parseFile(path);

	}//end of constructor


	//return x bytes from the internal buffer
	read(num) {
		let newPos = this.pos+num;
		if (newPos > this.data.length) newPos = this.data.length;
		const buf = this.data.slice(this.pos, newPos);
		this.pos = newPos;
		return buf;
	}

	resetStats() {
		this.speciesSeen = [];
		this.fursSeen = [];

		this.stats.rNone = 0;
		this.stats.rBronze = 0;
		this.stats.rSilver = 0;
		this.stats.rGold = 0;
		this.stats.rDiamond = 0;
		this.stats.TotalKilled = 0;
		this.stats.SpeciesVariations = 0;
		this.stats.FurVariations = 0;
		this.stats.HighestScore = 0;
		this.stats.LowestScore = 0;
		this.stats.unidentified = 0;
		
		for (const species of Object.keys(this.stats.species)) {
		  delete this.stats.species[species];
		}

		for (const fur of Object.keys(this.stats.furs)) {
			delete this.stats.furs[fur];
		}
	}
	ifNewSpecies(species) {
		if (typeof(species) === 'object') {			
			this.stats.unidentified++;//for debugging
			return;

		}
		if (this.speciesSeen.includes(species) === false) {
			//add new
			console.log(chalk.green.bold(`species ${species} new`));
			this.speciesSeen.push(species);
			this.stats.species[species] = 1;
			this.stats.SpeciesVariations++;
		} else {
			//existing
			this.stats.species[species]++;
		}
	}

	ifNewFurs(fur) {
		if (typeof(fur) === 'object') {
			return;
		}

		if (this.fursSeen.includes(fur) === false) {
			//add new
			console.log(chalk.green.bold(`fur ${fur} new`));
			this.fursSeen.push(fur);
			this.stats.furs[fur] = 1;
			this.stats.FurVariations++;
		} else {
			//existing
			this.stats.furs[fur]++;
		}
	}

	parseFile(path) {
		readFile(path)
		.then((fd) => {
			this.data = fd;
			console.log(fd);

			//latest

			this.pos += 0x5a;
			this.section = 'LATEST';
			this.readSect();
			console.log(chalk.red('----------------------------------------------------------------'));
			this.resetStats();
			
			//top 40 (approx)
			this.section = 'TOP';
			this.readHistory();

		console.log(this.stats);
		})
		.catch((e) => {
			console.error(e);
		});
	}
	readEntry() {
		console.log(`pos: ${this.pos}`);
		let d1 = new Date(0);
		const struct = {
			species: this.parseAnimal(this.read(4)),
			score: this.read(4).readInt16LE(0),
			rating: this.parseRating(this.read(4).readInt16LE(0)),
			fur: this.parseFur(this.read(4))
		}
		this.stats.TotalKilled++;
		this.ifNewSpecies(struct.species);
		this.ifNewFurs(struct.fur);

		//https://github.com/nodejs/node/issues/7074 <-- setSeconds() is incorrect, force UTC to temp fix #last test node v9.4.0
		d1.setUTCSeconds(this.read(8).readInt32LE(0))
		struct.time = d1;

		return struct;
	}
	readSect() {
		let num = parseInt(this.read(8).readInt32LE(0));
		for (let i = 0; i < num; i++) {
			let n = this.readEntry();
			
			console.log(`{ species:`, n.species);
			console.log(`  score:`, n.score);
			console.log(`  rating:`, n.rating);
			console.log(`  fur:`, n.fur);
			console.log(`  time:`, n.time.toISOString(), '}')
			
		}
	}
	readHistory() {
		let num = parseInt(this.read(8).readInt32LE(0));
		for (let i = 0; i < num; i++) {
			log1.debug(this.read(4));
			this.readSect();
		}
	}
	
	parseAnimal(buf) {
		//this.stats.TotalKilled++;
		switch(buf.toString('hex').toUpperCase()) {
			case 'AE5410A3':
				return 'Roosevelt Elk';

			case '38E6ADFA':
				return 'Moose';

			case 'F17DF8C8':
				return 'Blacktail Deer';

			case '74CD6096':
				return 'Coyote';

			case '7BBA167F':
				return 'White-tailed Jackrabbit';

			case 'CA739A5E':
				return 'Black Bear';

			case '0D963AC5':
				return 'Mallard';

			case 'D2030843':
				return 'Whitetail Deer';

			case 'D63FCA89':
				return 'Siberian Musk Deer';
			
			case '1A714D30':
				return 'Eurasian Lynx';
			
			case 'A0279B39':
				return 'Brown Bear';

			case '36B6BC93':
				return 'Reindeer';
			
			case '13ED0B96':
				return 'Roe Deer';
			/*
			case 'E0378D82':
				return '';

			

			case '969FFA26':
				return '';
			*/
			default:
				log1.debug(chalk.red.bold(`Species@${this.pos}: <${buf.toString('hex').match(/../g).join(' ')}> defaulted`));
				return buf;
		}
	}
	parseFur(buf) {
		switch(buf.toString('hex').toUpperCase()) {
			case '17907EDD':
				return 'Common';

			case '5AAAFA11':
				return 'Brown';

			case '279681B0':
				return 'Cinnamon';

			case '16E58EE4':
				return 'Piebald';

			case '760401E7':
				return 'Light Brown';
			
			case '4937A5A7':
				return 'Grey';

			case 'EA9DDCFD':
				return 'Red Brown';
			
			case '5CC5CDBD':
				return 'Blond';
/*
			

			case '':
				return '';

			case '':
				return '';

			case '':
				return '';

			*/	
			default:
				log1.debug(chalk.red.bold(`Fur@${this.pos}: <${buf.toString('hex').match(/../g).join(' ')}> defaulted`));
				return buf;
		}
	}
	parseRating(num) {
		switch (num) {
			case 0:
				this.stats.rDiamond++;
				return 'Diamond';

			case 1:
				this.stats.rGold++;
				return 'Gold';

			case 2:
				this.stats.rSilver++;
				return 'Silver';

			case 3:
				this.stats.rBronze++;
				return 'Bronze';

			default:
				this.stats.rNone++;
				return 'None';//case 4
		}
	}
}

const r = new HCoW(path);